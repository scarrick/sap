/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   SAP
 *  Class      :   MainFrame.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 25, 2021 @ 12:22:58 PM
 *  Modified   :   Oct 25, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 25, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.framework.api;

import java.awt.event.WindowEvent;

/**
 * The `MainFrame` has the name `mainFrame` and can be retrieved from the
 * `MainFrameApplication` subclass by calling `getMainFrame()`:
 * 
 * ```java
 * class MyClass extends MainFrameApplication {
 *     @Override
 *     protected void startup() {
 *         show(getMainFrame());
 *     }
 * }
 * ```
 * 
 * Since the `MainFrame` class provides everything needed for the base of a
 * Swing desktop application, the need to get the `mainFrame` from the
 * application is a rare thing. While the `MainFrame` provides a `menuBar`,
 * `toolBar`, and `statusBar`, there are no default menus on the menubar and
 * only a single button on the toolbar, which is for exiting the application.
 * 
 * However, the `MainFrame` provides more UI features than just those listed
 * above. It also provides:
 * 
 * | Feature | Use |
 * | :------ | :-- |
 * | `statusMessage` | The `statusMessage` is used to display information to the user. The status message text disappears after a time-limit is reached, which can be set in the `MainFrame.properties` `ResourceBundle` by setting `statusMessage.timeout` to a positive value. |
 * | `versionLabel` | The `versionLabel` displays the application's version. This text is set by the `versionLabel.text` property in the `MainFrame.properties` file. |
 * | `progressBar` | The `progressBar` displays the progress of `Task`s running in the background. When started with a `TaskService`, `Task`s can update the `statusMessage` text and the `progressBar` value. |
 * | `animationStatusLabel` | This label displays an idle icon, unless and until a `Task` is executed. By seeing the animated icon, as well as updates to the `progressBar` and `statusMessage`, users will know if background `Task`s are running or not. |
 * 
 * The `MainFrame` provides a `TaskService` to which custom background `Task`s
 * may be added to, and removed from, in order to have them executed. This
 * allows long-running and/or processor-intensive tasks to be moved off of
 * the Event Dispatching Thread (EDT), which will keep the application from
 * becoming unresponsive.
 * 
 * @see MainFrameApplication
 * @see Task
 * @see TaskService
 * @see TaskEvent
 * @see TaskMonitor
 * @see ResourceMap
 * @see Resource
 *
 * @author Sean Carrick
 */
class MainFrame extends javax.swing.JFrame {

    private final MainFrameApplication application;
    
    /**
     * Constructs a `MainFrame` for the `MainFrameApplication`.
     * 
     * @param application the `MainFrameApplication` in which this `MainFrame`
     * is being used
     */
    public MainFrame(Application application) {
        this.application = (MainFrameApplication) application;
        
        initComponents();
        
        generateMenus();
        generateToolbarButtons();
    }
    
    /**
     * Sets the text of the `statusMessage` on the status bar of the main window.
     * Either a plain `message` may be sent or the key to a resource may be 
     * sent to this method. If a key is sent, then `messageIsKey` needs to be
     * set to `true`.
     * 
     * @param message either the plain text message or a resource key
     * @param messageIsKey if `message` contains a resource key, then
     * `messageIsKey` needs to be true
     */
    public void setStatusMessage(String message, boolean messageIsKey) {
        if (messageIsKey) {
            statusMessage.setText(application.getContext().getResourceMap().getString(message));
        } else {
            statusMessage.setText(message);
        }
    }
    
    @Action
    public void quit() {
        application.shutdown();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        statusBar = new javax.swing.JPanel();
        statusMessage = new javax.swing.JLabel();
        animationStatusLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        jSeparator1 = new javax.swing.JSeparator();
        versionLabel = new javax.swing.JLabel();
        toolBar = new javax.swing.JToolBar();
        quitButton = new javax.swing.JButton();
        desktopPane = new com.gs.framework.api.TabbingDesktopPane();
        menuBar = new javax.swing.JMenuBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(100, 48));

        statusBar.setMaximumSize(new java.awt.Dimension(32767, 17));
        statusBar.setMinimumSize(new java.awt.Dimension(922, 17));
        statusBar.setName("statusBar"); // NOI18N
        statusBar.setPreferredSize(new java.awt.Dimension(922, 17));

        statusMessage.setText("Watch here for application information...");
        statusMessage.setMaximumSize(new java.awt.Dimension(254, 16));
        statusMessage.setMinimumSize(new java.awt.Dimension(254, 16));
        statusMessage.setName("statusMessage"); // NOI18N
        statusMessage.setPreferredSize(new java.awt.Dimension(254, 16));

        animationStatusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/framework/api/resources/icons/busyicons/idle-icon.png"))); // NOI18N
        animationStatusLabel.setName("animationStatusLabel"); // NOI18N

        progressBar.setMaximumSize(new java.awt.Dimension(146, 16));
        progressBar.setMinimumSize(new java.awt.Dimension(146, 16));
        progressBar.setName("progressBar"); // NOI18N
        progressBar.setPreferredSize(new java.awt.Dimension(146, 16));

        versionLabel.setText("Application Version");
        versionLabel.setName("versionLabel"); // NOI18N

        javax.swing.GroupLayout statusBarLayout = new javax.swing.GroupLayout(statusBar);
        statusBar.setLayout(statusBarLayout);
        statusBarLayout.setHorizontalGroup(
            statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusBarLayout.createSequentialGroup()
                .addComponent(statusMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(versionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 359, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(animationStatusLabel))
        );
        statusBarLayout.setVerticalGroup(
            statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusBarLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(versionLabel)
                    .addGroup(statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(statusMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(animationStatusLabel))
                        .addComponent(jSeparator1))))
        );

        getContentPane().add(statusBar, java.awt.BorderLayout.PAGE_END);

        toolBar.setRollover(true);
        toolBar.setName("toolBar"); // NOI18N

        quitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/framework/api/resources/icons/large/stock_exit24.png"))); // NOI18N
        quitButton.setFocusable(false);
        quitButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        quitButton.setName("quitButton"); // NOI18N
        quitButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(quitButton);

        getContentPane().add(toolBar, java.awt.BorderLayout.PAGE_START);

        desktopPane.setName("desktop"); // NOI18N
        getContentPane().add(desktopPane, java.awt.BorderLayout.CENTER);

        menuBar.setName("menuBar"); // NOI18N
        setJMenuBar(menuBar);
        // Getting the overridable method call out of the constructor.

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void generateMenus() {
        // TODO: Implement MainFrame.generateMenus() to create menus from @Menu and @MenuItem decorations
    }
    
    private void generateToolbarButtons() {
        // TODO: Implement MainFrame.generateToolbarButtons() to create toolbar buttons from @ButtonProvider decorations
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel animationStatusLabel;
    private com.gs.framework.api.TabbingDesktopPane desktopPane;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton quitButton;
    private javax.swing.JPanel statusBar;
    private javax.swing.JLabel statusMessage;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JLabel versionLabel;
    // End of variables declaration//GEN-END:variables
}
