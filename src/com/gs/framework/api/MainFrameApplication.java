/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   SAP
 *  Class      :   MainFrameApplication.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 25, 2021 @ 11:49:19 AM
 *  Modified   :   Oct 25, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 25, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.framework.api;

import com.gs.framework.utils.TerminalErrorPrinter;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.RootPaneContainer;

/**
 * An application base class for GUI applications with a single primary frame,
 * but which supports docking child windows.
 * 
 * This class takes care of component property injection, exit processing, and
 * saving/restoring session state in a way that is appropriate for single-frame
 * applications. The application's JFrame is created automatically with a 
 * WindowListener that calls `exit()` when the window is closed. Session state
 * is stored when the application shuts down, and restored when the GUI is 
 * shown.
 * 
 * To use `MainFrameApplication`, one need only override `startup`, create the
 * GUI's main panel, and apply `show` to that. Here is an example
 * 
 * ```java
 * class MyApplication extends MainFrameApplication {
 *     @Override
 *     protected void startup() {
 *         show(new JLabel("Hello World"));
 *     }
 * }
 * ```
 * 
 * The call to `show` in this example creates a `javax.swing.JFrame` (named
 * `mainFrame`), that contains the "Hello World" `JLabel`. Before the frame is
 * made visible, the properties of all of the components in the hierarchy are
 * initialized with [ResourceMap.injectComponents](ResourceMap#injectComponents)
 * and then restored from saved session state (if any) with
 * [SessionStorage.restore](SessionStorage#restore). When the application shuts
 * down, session state is saved.
 * 
 * A more realistic tiny example would rely on a `ResourceBundle` for the 
 * `JLabel`'s string and the main frame's title. The automatic injection step
 * only initializes the properties of named components, so:
 * 
 * ```java
 * class MyApplication extends MainFrameApplication {
 *     @Override
 *     protected void startup() {
 *         JLabel label = new JLabel();
 *         label.setName("label");
 *         show(label);
 *     }
 * }
 * ```
 * 
 * The `ResourceBundle` should contain definitions for all of the standard
 * `Application` resources, as well as the main frame's title and the label's
 * text. note that the `JFrame` that is implicitly created by the `show` method
 * is named mainFrame.
 * 
 * ```
 * # resources/MyApplication.properties
 * 
 * Application.id = MyApplication
 * Application.title = My Hello World Application
 * Application.version = 1.0
 * Application.vendor = GS United Labs
 * Application.vendorId = gs
 * Application.homepage = https://gs-unitedlabs.com
 * Application.description = An example of MainFrameApplication
 * Application.lookAndFeel = system
 * 
 * mainFrame.title = ${Application.title} ${Application.version}
 * label.text = Hello World
 * ```
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class MainFrameApplication extends Application {
    
    private static final Logger logger = Logger.getLogger(MainFrameApplication.class.getName());
    private ResourceMap appResources = null;
    private JFrame mainFrame;

    /**
     * Return the JFrame used to show this application.
     * 
     * The frame's name is set to "mainFrame", its title is initialized with the
     * value of the `Application.title` resource and a `WindowListener` is added
     * that calls `exit` when the user attempts to close the frame.
     * 
     * This method may be called at any time; the `JFrame` is created lazily and
     * cached. For example:
     * 
     * ```java
     * @Override
     * protected void startup() {
     *     getMainFrame.setJMenuBar(createMenuBar());
     *     show(createMainPanel());
     * }
     * ```
     * 
     * @return this `Application`'s main frame
     * 
     * @see #setMainFrame(JFrame)
     * @see #show()
     * @see javax.swing.JFrame#setName(java.lang.String) 
     * @see javax.swing.JFrame#setTitle(java.lang.String) 
     * @see javax.swing.JFrame#addWindowListener(java.awt.event.WindowListener) 
     */
    public final JFrame getMainFrame() {
        if (mainFrame == null) {
            mainFrame = new MainFrame(getContext().getApplication());
        }
        
        return mainFrame;
    }
    
    private String sessionFileName(Component window) {
        if (window == null) {
            return null;
        } else {
            String name = window.getName();
            return (name == null) ? null : name + "session.xml";
        }
    }

    /**
     * Initialize the hierarchy with the specified root by injecting resources.
     * 
     * By default the `show` methods [inject resources](ResourceMap#injectComponents)
     * before initializing the JFrame's or JDialog's size, location, and restoring
     * the window's session state. If the app is showing a window whose resources
     * have already been injected, or that should not be initialized via resource
     * injection, this method can be overridden to defeat the default behavior.
     * 
     * @param root the root of the component hierarchy
     * 
     * @see ResourceMap#injectComponents(java.awt.Component) 
     * @see #show() 
     * @see #show(javax.swing.JFrame) 
     * @see #show(javax.swing.JDialog)
     * @see #show(javax.swing.Dialog) 
     * @see #show(javax.swing.Window) 
     * @see #show(javax.swing.AboutPanel)
     * @see #show(javax.swing.OptionsPanel)
     */
    protected void configureWindow(Window root) {
        getContext().getResourceMap().injectComponents(root);
    }
    
    protected void configureWindow(java.awt.Window window) {
        getContext().getResourceMap().injectComponents(window);
    }
    
    private void initRootPaneContainer(RootPaneContainer c) {
        JComponent rootPane = c.getRootPane();
        
        // These initializations are only done once.
        Object k = "MainFrameApplication.initRootPaneContainer";
        if (rootPane.getClientProperty(k) != null) {
            return;
        }
        
        rootPane.putClientProperty(k, Boolean.TRUE);
        
        // Inject resources
        Container root = rootPane.getParent();
        if (root instanceof Window) {
            configureWindow((Window) root);
        } else if (root instanceof java.awt.Window) {
            configureWindow((java.awt.Window) root);
        }
        
        // If this is the mainFrame, then close == exit.
        JFrame mainFrame = getMainFrame();
        if (c == mainFrame) {
            mainFrame.addWindowListener(new MainFrameListener());
            mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        } else if (root instanceof Window) { // close == save session state
            Window window = (Window) root;
            window.addHierarchyListener(new SecondaryWindowListener());
        }
        
        // If this is a JFrame, monitor "normal" (not maximized) bounds.
        if (root instanceof JFrame) {
            root.addComponentListener(new FrameBoundsListener());
        }
        
        // If the window's bounds do not appear to have been set, do it.
        if (root instanceof java.awt.Window) {
            java.awt.Window window = (java.awt.Window) root;
            if (!root.isValid() || (root.getWidth() == 0) || (root.getHeight() == 0)) {
                window.pack();
            }
            
            if (!window.isLocationByPlatform() && (root.getX() == 0) && (root.getY() == 0)) {
                Component owner = window.getOwner();
                
                if (owner == null) {
                    owner = (window != mainFrame) ? mainFrame : null;
                }
                
                window.setLocationRelativeTo(owner); // center the window
            }
        }
        
        if (root instanceof Window) {
            Window window = (Window) root;
            
            if (!root.isValid() || (root.getWidth() == 0) || (root.getHeight() == 0)) {
                window.pack();
            }
        }
        
        // Restore session state.
        if (root instanceof Window) {
            String filename = sessionFileName((Window) root);
            
            if (filename != null) {
                try {
                    getContext().getSessionStorage().restore(root, filename);
                } catch (Exception e) {
                    String msg = String.format("could not restore session [%s]", filename);
                    TerminalErrorPrinter.print(e, msg);
                    logger.log(Level.WARNING, msg);
                }
            }
        }
    }
    
    /**
     * Show the `MainFrame` for the application. Typical applications will call
     * this method after performing other GUI configuration in the `startup`
     * method.
     * 
     * Before the main frame is made visible, the properties of all of the 
     * components in the hierarchy are initialized with 
     * [ResourceMap.injectComponents](ResourceMap#injectComponents) and then
     * restored from saved session state (if any) with the
     * [SessionStorage.restore](SessionStorage#restore) method. When the 
     * application shuts down, session state is saved.
     * 
     * Note that the name of the lazily created main frame (see
     * [getMainFrame](#getMainFrame)) is set by default. Session state is only 
     * saved for top-level windows and dockables with a valid name, and then
     * only for component descendants that are named.
     */
    protected void show() {
        JFrame f = getMainFrame();
        initRootPaneContainer(f);
        f.setVisible(true);
    }
    
    
    /**
     * Initialize and show the Dialog.
     * 
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the `mainFrame`, dismissing a 
     * secondary window will not exit the application.
     * 
     * Session state is only automatically saved if the specified `Dialog` has
     * a name, and then only for component descendants that are named.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `Dialog` to be shown
     */
    public void show(Dialog c) {
        if (c == null) {
            throw new IllegalArgumentException("null Dialog");
        }
        initRootPaneContainer(c);
        show(c);
    }
    
    /**
     * Initialize and show the JDialog.
     * 
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the `mainFrame`, dismissing a 
     * secondary window will not exit the application.
     * 
     * Session state is only automatically saved if the specified `JDialog` has
     * a name, and then only for component descendants that are named.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `JDialog` to be shown
     */
    public void show(JDialog c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        
        initRootPaneContainer(c);
        c.setVisible(true);
    }
    
    /**
     * Initialize and show the JFrame.
     * 
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the `mainFrame`, dismissing a 
     * secondary window will not exit the application.
     * 
     * Session state is only automatically saved if the specified `JFrame` has
     * a name, and then only for component descendants that are named.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `JFrame` to be shown
     */
    public void show(JFrame c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        
        initRootPaneContainer(c);
        c.setVisible(true);
    }
    
    /**
     * Initialize and show the Window.
     * 
     * This method is intended for showing "docking" windows, like editors, 
     * navigators, and so on. Unlike the `mainFrame`, closing a docking window
     * does not exit the application. However, a quality-designed docking window
     * will prompt the user to save unsaved modifications prior to closing. See
     * [DockingWindowAdapter](DockingWindowAdapter) to learn how to do this.
     * 
     * Session state is only automatically saved if the specified `Window` has
     * a name, and then only for component descendants that are named.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `Window` to be shown
     */
    public void show(Window c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        
        initRootPaneContainer(c);
        
        // TODO: Implement docking logic based upon the @WindowPosition decoration.
        
        c.setVisible(true);
    }
    
    /**
     * Initialize and show the AboutPanel.
     * 
     * This method is intended for showing additional About panels for individual
     * modules that make up the totality of the application. Each module can
     * provide its own about information for the About box that is provided by
     * the application platform.
     * 
     * Session state does not apply for `AboutPanel`s, so they only have their
     * resources injected without going through the session restoration process.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `AboutPanel` to be shown
     */
    public void show(AboutPanel c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        
        getContext().getResourceMap().injectComponents(c);
        
        // TODO: Implement the logic for adding AboutPanels to the AboutBox.
    }
    
    /**
     * Initialize and show the OptionsPanel.
     * 
     * This method is intended for showing application-specific tabs in the
     * Options dialog. Each application has its own custom options that it is
     * interested in, so applications can provide `OptionsPanel`s to allow the
     * user to set those options.
     * 
     * Session state does not apply for `OptionPanel`s, so they only have their
     * resources injected without going through the session restoration process.
     * 
     * Throws an `IllegalArgumentException` if `c` is `null`.
     * 
     * @param c the `OptionsPanel` to be shown
     */
    public void show(OptionsPanel c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        
        getContext().getResourceMap().injectComponents(c);
        
        // TODO: Implement the logic for adding OptionsPanels to the OptionsDialog.
    }
    
    private void saveSession(Component window) {
        String filename = sessionFileName(window);
        
        if (filename != null) {
            try {
                getContext().getSessionStorage().save(window, filename);
            } catch (IOException e) {
                String msg = String.format("could not save session for %s", 
                        window);
                TerminalErrorPrinter.print(e, msg);
                logger.log(Level.WARNING, msg, e);
            }
        }
    }
    
    private boolean isVisibleWindow(Component w) {
        return w.isVisible() && ((w instanceof Window) || (w instanceof Dialog)
                || (w instanceof JFrame) || (w instanceof JDialog)
                || (w instanceof JWindow));
    }
    
    /**
     * Return all of the visible Windows per `TabbingDesktopPane.getAllFrames()`,
     * since we are only faking windows by using the `Window` class which is
     * a subclass of `JInternalFrame`.
     * 
     * @return a `java.util.List` of all open `Window`s
     */
    private List<Window> getVisibleSecondaryWindows() {
        List<Window> rv = new ArrayList<>();
        Method getWindowsM = null;
        
        try {
            getWindowsM = TabbingDesktopPane.class.getMethod("getAllFrames");
        } catch (NoSuchMethodException ignore) {}
        
        if (getWindowsM != null) {
            Window[] windows = null;
            
            try {
                windows = (Window[]) getWindowsM.invoke(null);
            } catch (IllegalAccessException | IllegalArgumentException 
                    | InvocationTargetException e) {
                throw new Error("HCTB - cannot get top level windows list", e);
            }
            
            if (windows != null) {
                for (Window window : windows) {
                    if (isVisibleWindow(window)) {
                        rv.add(window);
                    }
                }
            }
        }
        
        return rv;
    }
    
    /**
     * Save session state for the component hierarchy rooted by the `mainFrame`.
     * `MainFrameApplication` subclasses that override `shutdown` need to 
     * remember to call `super.shutdown()`.
     */
    @Override
    protected void shutdown() {
        saveSession(getMainFrame());
        
        for (Window window : getVisibleSecondaryWindows()) {
            saveSession(window);
        }
    }
    
    private class MainFrameListener extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            exit(e);
        }
    }
    
    /* Although it would have been simpler to listen for changes in the secondary
     * window's visibility per either a PropertyChangeEvent on the "visible"
     * property or a change in visibility per ComponentListener, neither listener
     * is notified if the secondary window is disposed. HierarchyEvent.SHOWING_CHANGED
     * does report the change in all cases, so we use that.
     */
    private class SecondaryWindowListener implements HierarchyListener {

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                if (e.getSource() instanceof Window) {
                    Window secondaryWindow = (Window) e.getSource();
                    if (!secondaryWindow.isShowing()) {
                        saveSession(secondaryWindow);
                    }
                }
            }
        }
        
    }
    
    /* In order to properly restore a maximized JFrame, we need to record its
     * normal (not maximized) bounds. They are recorded under a rootPane client
     * property here, so that they can be session-saved by 
     * WindowProperty#getSessionState().
     */
    private static class FrameBoundsListener implements ComponentListener {
        
        private void maybeSaveFrameSize(ComponentEvent e) {
            if (e.getComponent() instanceof JFrame) {
                JFrame f = (JFrame) e.getComponent();
                
                if ((f.getExtendedState() & Frame.MAXIMIZED_BOTH) == 0) {
                    String clientPropertyKey = "WindowState.normalBounds";
                    f.getRootPane().putClientProperty(clientPropertyKey, 
                            f.getBounds());
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            maybeSaveFrameSize(e);
        }

        /* ******************************************************************
           * Back in the WinXP days, there was a bug with JDK6 that caused  *
           * this method to be called once when the frame is maximized with *
           * x,y == -4 and getExtendedState == 0. Since both JDK6 and WinXP *
           * have gone far, far away, I went ahead and uncommented the line *
           * in the componentMoved() method.                                *
           ****************************************************************** */
        @Override
        public void componentMoved(ComponentEvent e) {
            maybeSaveFrameSize(e);
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }
        
    }
    
    /* **********************************************************************
       *     A NOTE REGARDING VIEW TYPES AND THE PROTOTYPE SHOW METHODS     *
       **********************************************************************
       * Since I have decided not to reinstitute the View and FrameView     *
       * classes, even though Hans had suggested that they might make it    *
       * easy to implement for docking layouts, I have not included the two *
       * prototype methods for showing the View types.                      *
       *                                                                    *
       * If the current docking solution completely fails to meet our needs,*
       * we could work on trying to figure out how easily the View types can*
       * be used for creating a docking layout interface. I am keeping those*
       * original classes on my local computer, so we can always add them   *
       * back in later.                                                     *
       *                                                                    *
       *                                                       Sean Carrick *
       *                                                       OCT 25, 2021 *
       ********************************************************************** */

}
