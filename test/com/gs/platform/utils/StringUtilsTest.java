/*
 * Copyright (C) 2021 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   SAP
 *  Class      :   StringUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 24, 2021 @ 9:38:56 AM
 *  Modified   :   Oct 24, 2021
 * 
 *  Purpose:     See class JavaDoc comment.
 * 
 *  Revision History:
 * 
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 24, 2021  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.platform.utils;

import com.gs.framework.utils.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class StringUtilsTest {
    
    public StringUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of wrap method, of class StringUtils.
     */
    @Test
    public void testWrap() {
        System.out.println("wrap");
        
        // Testing without newline characters nor hyphens.
        String source = "This is a test string for the wrap method. We need to "
                + "make sure that it wraps properly. This test is without any "
                + "newline characters.";
        int width = 80;
        String expResult = "This is a test string for the wrap method. We need to "
                + "make sure that it wraps\nproperly. This test is without any "
                + "newline characters.";
        String result = StringUtils.wrap(source, width);
        System.out.println("Testing the following text for proper wrapping, without newline characters nor hyphens:");
        System.out.println();
        System.out.println(source);
        System.out.println();
        System.out.println("The expected results are:");
        System.out.println();
        System.out.println(expResult);
        System.out.println("-".repeat(80));
        System.out.println("The actual results are:");
        System.out.println();
        System.out.println(result);
        assertEquals(expResult, result);
        
        // Testing without hyphens, but with a newline character.
        source = "This is a test string for the wrap method.\nWe need to "
                + "make sure that it wraps properly. This test contains a "
                + "newline character.";
        expResult = "This is a test string for the wrap method.\nWe need to "
                + "make sure that it wraps properly. This test contains a "
                + "newline\ncharacter.";
        result = StringUtils.wrap(source, width);
        System.out.println("Testing the following text for proper wrapping, with a newline character but no hyphens:");
        System.out.println();
        System.out.println(source);
        System.out.println();
        System.out.println("The expected results are:");
        System.out.println();
        System.out.println(expResult);
        System.out.println("-".repeat(80));
        System.out.println("The actual results are:");
        System.out.println();
        System.out.println(result);
        assertEquals(expResult, result.trim());
        
        // Testing with a newline character and a hyphen.
        source = "This is a test string for the wrap method.\nThis test "
                + "contains both a newline character and a hyphen or two. A "
                + "complex, hy-phenated word needs to have the hyphen inside of "
                + "the 80 character mark so that we can be sure that hyphenated "
                + "words will be properly broken.";
        expResult = "This is a test string for the wrap method.\nThis test "
                + "contains both a newline character and a hyphen or two. A "
                + "complex, hy-\nphenated word needs to have the hyphen inside "
                + "of the 80 character mark so that\nwe can be sure that "
                + "hyphenated words will be properly broken.";
        result = StringUtils.wrap(source, width);
        System.out.println("Testing the following text for proper wrapping, with newline character and a hyphen:");
        System.out.println();
        System.out.println(source);
        System.out.println();
        System.out.println("The expected results are:");
        System.out.println();
        System.out.println(expResult);
        System.out.println("-".repeat(80));
        System.out.println("The actual results are:");
        System.out.println();
        System.out.println(result);
        assertEquals(expResult, result.trim());
    }

    /**
     * Test of insertTabLeader method, of class StringUtils.
     */
    @Test
    public void testInsertTabLeader() {
        System.out.println("insertTabLeader");
        String leftWord = "OS";
        String rightWord = System.getProperty("os.name");
        int rightMargin = 40;
        char leader = '.';
        String expResult = "OS.................................Linux";
        String result = StringUtils.insertTabLeader(leftWord, rightWord, rightMargin, leader);
        System.out.println("=".repeat(80));
        System.out.println();
        System.out.println("This is a test for the StringUtils.insertTabLeader method.");
        System.out.println();
        System.out.println("leftWord\trightWord\trightMargin\tleader");
        System.out.println(leftWord + "\t\t" + rightWord + "\t\t" + rightMargin 
                + "\t\t" + leader);
        System.out.println();
        System.out.println("Expected results:");
        System.out.println();
        System.out.println(expResult);
        System.out.println();
        System.out.println("The results:");
        System.out.println();
        System.out.println(result);
        assertEquals(expResult, result);
    }
    
}
